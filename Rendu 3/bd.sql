DROP VIEW IF EXISTS livre;
DROP VIEW IF EXISTS film;
DROP VIEW IF EXISTS enregistrementAudio;
DROP VIEW IF EXISTS exemplaires_empruntes;
DROP TABLE IF EXISTS adhesion;
DROP TABLE IF EXISTS suspension;
DROP TABLE IF EXISTS emprunt;
DROP TABLE IF EXISTS exemplaire;
DROP TABLE IF EXISTS edition;
DROP TABLE IF EXISTS editeur;
DROP TABLE IF EXISTS contribution;
DROP TABLE IF EXISTS contributeur;
DROP TABLE IF EXISTS pays;
DROP TABLE IF EXISTS auteur;
DROP TABLE IF EXISTS realisateur;
DROP TABLE IF EXISTS compositeur;
DROP TABLE IF EXISTS interprete;
DROP TABLE IF EXISTS acteur;
DROP TABLE IF EXISTS administrateur;
DROP TABLE IF EXISTS ressource;
DROP TABLE IF EXISTS enregistrementAudio;
DROP TABLE IF EXISTS langue;
DROP TABLE IF EXISTS genre;
DROP TABLE IF EXISTS personne CASCADE;
DROP TYPE IF EXISTS Role;
DROP TYPE IF EXISTS Etat;
DROP TYPE IF EXISTS TypeContributeur;
DROP TYPE IF EXISTS TypeRessource;
DROP TYPE IF EXISTS Type;
DROP TYPE IF EXISTS Motif;
DROP TRIGGER IF EXISTS T_Contribution_Check_Types ON contribution;

create table pays(
    nom varchar(30) primary key
);

create table genre(
    nom varchar(30) primary key
);

create table langue(
    nom varchar(30) primary key
);

CREATE TYPE TypeRessource AS ENUM ('EnregistrementAudio', 'Film', 'Livre');

create table ressource(
    codeUnique SERIAL primary key,
    titre varchar(100) NOT NULL,
    dateApparition date NOT NULL,
    genre varchar(30) NOT NULL,
    type TypeRessource NOT NULL,
    langue varchar(30),
    isbn varchar(13),
    resume varchar(255),
    duree INTEGER,
    foreign key(genre) references genre(nom),
    foreign key(langue) references langue(nom)
);

create view enregistrementAudio as
    select * from ressource where type = 'EnregistrementAudio';

create view film as
    select * from ressource where type = 'Film';

create view livre as
    select * from ressource where type = 'Livre';

/*IF type = Film OR type = Livre THEN langue NOT NULL*/
alter table ressource
    add constraint check_langue check (type = 'EnregistrementAudio' OR langue IS NOT NULL);
alter table ressource
    add constraint check_duree check (type = 'Livre' OR duree IS NOT NULL);
alter table ressource
    add constraint check_isbn check (type = 'EnregistrementAudio' OR type = 'Film' OR isbn IS NOT NULL);
alter table ressource
    add constraint check_resume check (type = 'EnregistrementAudio' OR resume IS NOT NULL);

CREATE TYPE TypeContributeur AS ENUM ('Acteur', 'Interprete', 'Compositeur', 'Realisateur', 'Auteur');

create table contributeur(
    id SERIAL primary key,
    nom varchar(30) NOT NULL,
    prenom varchar(30) NOT NULL,
    dateNaissance date NOT NULL,
    pays varchar(30) NOT NULL,
    type TypeContributeur NOT NULL,
    foreign key (pays) references pays(nom),
    UNIQUE(nom, prenom, dateNaissance, type)
);

create table contribution(
    date date NOT NULL,
    contributeur INTEGER,
    ressource INTEGER,
    foreign key (contributeur) references contributeur(id),
    foreign key (ressource) references ressource(codeUnique),
    PRIMARY KEY (contributeur, ressource)
);

CREATE OR REPLACE FUNCTION Contribution_Check_Types()
RETURNS TRIGGER
LANGUAGE PLPGSQL
AS $function_contribution$
DECLARE
    typeC TEXT;
    typeR TEXT;
BEGIN
    SELECT type INTO typeC FROM contributeur WHERE id = NEW.contributeur;
    SELECT type INTO typeR FROM ressource WHERE codeUnique = NEW.ressource;

    IF typeR = 'Film' AND (typeC = 'Acteur' OR typeC = 'Realisateur') THEN
        RETURN NEW;
    ELSEIF typeR = 'EnregistrementAudio' AND (typeC = 'Interprete' OR typeC = 'Compositeur') THEN
        RETURN NEW;
    ELSEIF typeR = 'Livre' AND typeC = 'Auteur' THEN
        RETURN NEW;
    END IF;
    RAISE EXCEPTION 'Erreur : ce contributeur ne peut pas travailler sur ce type de ressources';
END;
$function_contribution$;

CREATE TRIGGER T_Contribution_Check_Types
    BEFORE INSERT
    ON contribution
    FOR EACH ROW
EXECUTE PROCEDURE Contribution_Check_Types();


create table editeur(
    nom varchar(30) primary key
);

create table edition(
    ressource INTEGER,
    editeur varchar(30),
    foreign key (ressource) references ressource(codeUnique),
    foreign key (editeur) references editeur(nom),
    PRIMARY KEY (ressource, editeur)
);

CREATE TYPE Etat AS ENUM ('neuf', 'bon', 'abîmé', 'perdu');

create table exemplaire(
    codeUnique SERIAL primary key ,
    ressource INTEGER NOT NULL,
    etat Etat NOT NULL,
    codePosition varchar(30) NOT NULL,
    foreign key (ressource) references ressource(codeUnique),
    UNIQUE(codePosition)
);

create TYPE Role AS ENUM ('adherent', 'administrateur');

create table personne(
    login varchar(30) primary key,
    password varchar(30) NOT NULL,
    email varchar(30) NOT NULL,
    adresse varchar(100) NOT NULL,
    nom varchar(30) NOT NULL,
    prenom varchar(30) NOT NULL,
    role Role NOT NULL,
    dateNaissance date,
    telephone char(10),
    UNIQUE(nom, prenom, adresse),
    UNIQUE(email),
    UNIQUE(telephone)
);

/*IF role = adherent THEN telephone KEY (UNIQUE & NOT NULl)*/
alter table personne
    add constraint check_telephone check (role <> 'adherent' OR telephone IS NOT NULL);

create view adherent as
select * from personne where role = 'adherent';

create table emprunt(
    dateEmprunt date,
    exemplaire INTEGER,
    adherent varchar(30),
    dateRendu date,
    duree INTEGER NOT NULL,
    foreign key (exemplaire) references exemplaire(codeUnique),
    foreign key (adherent) references personne(login),
    PRIMARY KEY (dateEmprunt, exemplaire, adherent)
);

/*dateRendu IS NULL OR dateRendu >= dateEmprunt*/
alter table emprunt
    add constraint check_dateRendu check (dateRendu IS NULL OR dateRendu >= dateEmprunt);

create view exemplaires_empruntes as
    select * from exemplaire where exemplaire.codeUnique in (select exemplaire from emprunt where emprunt.dateRendu IS NULL);

/*exemplaire.etat <> abîmé AND exemplaire.etat <> perdu*/


/*Un adherent ne peut avoir qu'un nombre limité d'emprunts simultanés (3) (emprunt en cours : dateRendu IS NULL)*/

CREATE TYPE Motif AS ENUM ('dégradation', 'perte', 'retard', 'blacklist');

create table suspension(
    dateDebut date,
    adherent varchar(30),
    dateFin date,
    motif Motif NOT NULL,
    foreign key (adherent) references personne(login),
    PRIMARY KEY (dateDebut, adherent)
);

/*dateFin IS NULL OR dateFin >= dateDebut*/
alter table suspension
    add constraint check_dateFin_suspension check (dateFin IS NULL OR dateFin >= dateDebut);

create table adhesion(
    dateDebut date,
    adherent varchar(30),
    dateFin date,
    foreign key (adherent) references personne(login),
    PRIMARY KEY (dateDebut, adherent)
);

/*dateFin IS NULL OR dateFin >= dateDebut*/
alter table adhesion
    add constraint check_dateFin_adhesion check (dateFin IS NULL OR dateFin >= dateDebut);


INSERT INTO pays VALUES ('France');
INSERT INTO pays VALUES ('Allemagne');
INSERT INTO pays VALUES ('Angleterre');
INSERT INTO pays VALUES ('Espagne');
INSERT INTO pays VALUES ('Italie');
INSERT INTO pays VALUES ('Etats-Unis');
INSERT INTO pays VALUES ('Canada');
INSERT INTO pays VALUES ('Japon');
INSERT INTO pays VALUES ('Chine');
INSERT INTO pays VALUES ('Russie');
INSERT INTO pays VALUES ('Nouvelle-Zélande');
INSERT INTO pays VALUES ('Ecosse');
INSERT INTO pays VALUES ('Irlande');

INSERT INTO genre VALUES ('Action');
INSERT INTO genre VALUES ('Aventure');
INSERT INTO genre VALUES ('Comédie');
INSERT INTO genre VALUES ('Drame');
INSERT INTO genre VALUES ('Fantastique');
INSERT INTO genre VALUES ('Horreur');
INSERT INTO genre VALUES ('Policier');
INSERT INTO genre VALUES ('Romance');
INSERT INTO genre VALUES ('Science-fiction');
INSERT INTO genre VALUES ('Thriller');

INSERT INTO langue VALUES ('Français');
INSERT INTO langue VALUES ('Anglais');
INSERT INTO langue VALUES ('Allemand');
INSERT INTO langue VALUES ('Espagnol');
INSERT INTO langue VALUES ('Italien');
INSERT INTO langue VALUES ('Japonais');
INSERT INTO langue VALUES ('Chinois');
INSERT INTO langue VALUES ('Russe');

INSERT INTO ressource (titre, dateApparition, genre, type, langue, resume, duree) VALUES ('Le seigneur des anneaux', '2001-12-19', 'Fantastique', 'Film', 'Anglais', 'resume du film', 178);
INSERT INTO ressource (titre, dateApparition, genre, type, langue, resume, duree) VALUES ('Game of Thrones', '2011-04-17', 'Fantastique', 'Film', 'Anglais', 'resume du film', 138);
INSERT INTO ressource (titre, dateApparition, genre, type, langue, resume, duree) VALUES ('Harry Potter', '2001-11-16', 'Fantastique', 'Film', 'Anglais', 'resume du film', 152);
INSERT INTO ressource (titre, dateApparition, genre, type, langue, resume, duree) VALUES ('Avengers', '2012-04-25', 'Action', 'Film', 'Anglais', 'resume du film', 143);

INSERT INTO ressource (titre, dateApparition, genre, type, langue, isbn, resume) VALUES ('Le seigneur des anneaux', '1954-07-29', 'Fantastique', 'Livre', 'Anglais', '2-7654-1005-4', 'resume du livre');
INSERT INTO ressource (titre, dateApparition, genre, type, langue, isbn, resume) VALUES ('Game of Thrones', '1996-08-06', 'Fantastique', 'Livre', 'Anglais', '2-7654-1005-5', 'resume du livre');
INSERT INTO ressource (titre, dateApparition, genre, type, langue, isbn, resume) VALUES ('Gaston Lagaffe 1', '1957-03-28', 'Comédie', 'Livre', 'Français', '2-7654-1005-6', 'resume du livre');
INSERT INTO ressource (titre, dateApparition, genre, type, langue, isbn, resume) VALUES ('Gaston Lagaffe 2', '1981-10-28', 'Comédie', 'Livre', 'Français', '2-7654-1005-7', 'resume du livre');

INSERT INTO ressource (titre, dateApparition, genre, type, duree)  VALUES ('Le seigneur des anneaux', '1978-11-15', 'Fantastique', 'EnregistrementAudio', 29);
INSERT INTO ressource (titre, dateApparition, genre, type, duree)  VALUES ('Podcast sur UTC', '2023-12-17', 'Aventure', 'EnregistrementAudio', 34);

INSERT INTO contributeur VALUES (DEFAULT, 'Peter', 'Jackson', '1961-10-31', 'Nouvelle-Zélande', 'Realisateur');
INSERT INTO contributeur VALUES (DEFAULT, 'George', 'Martin', '1948-09-20', 'Etats-Unis', 'Auteur');
INSERT INTO contributeur VALUES (DEFAULT, 'J.K', 'Rowling', '1965-07-31', 'Angleterre', 'Auteur');
INSERT INTO contributeur VALUES (DEFAULT, 'J.R.R', 'Tolkien', '1892-01-03', 'Angleterre', 'Auteur');
INSERT INTO contributeur VALUES (DEFAULT, 'Sean', 'Bean', '1959-04-17', 'Angleterre', 'Acteur');
INSERT INTO contributeur VALUES (DEFAULT, 'Elijah', 'Wood', '1981-01-28', 'Etats-Unis', 'Acteur');
INSERT INTO contributeur VALUES (DEFAULT, 'Ian', 'McKellen', '1939-05-25', 'Angleterre', 'Acteur');
INSERT INTO contributeur VALUES (DEFAULT, 'Viggo', 'Mortensen', '1958-10-20', 'Etats-Unis', 'Acteur');
INSERT INTO contributeur VALUES (DEFAULT, 'Orlando', 'Bloom', '1977-01-13', 'Angleterre', 'Acteur');
INSERT INTO contributeur VALUES (DEFAULT, 'Sean', 'Astin', '1971-02-25', 'Etats-Unis', 'Acteur');
INSERT INTO contributeur VALUES (DEFAULT, 'Dominic', 'Monaghan', '1976-12-08', 'Allemagne', 'Acteur');
INSERT INTO contributeur VALUES (DEFAULT, 'Billy', 'Boyd', '1968-08-28', 'Ecosse', 'Acteur');

INSERT INTO editeur VALUES ('Gallimard');
INSERT INTO editeur VALUES ('Hachette');
INSERT INTO editeur VALUES ('Larousse');
INSERT INTO editeur VALUES ('Nathan');
INSERT INTO editeur VALUES ('Hatier');
INSERT INTO editeur VALUES ('Bordas');
INSERT INTO editeur VALUES ('Dunod');
INSERT INTO editeur VALUES ('Eyrolles');
INSERT INTO editeur VALUES ('Flammarion');
INSERT INTO editeur VALUES ('Fayard');

INSERT INTO exemplaire VALUES (DEFAULT, 1, 'neuf', 'A1');
INSERT INTO exemplaire VALUES (DEFAULT, 1, 'bon', 'A2');
INSERT INTO exemplaire VALUES (DEFAULT, 1, 'abîmé', 'A3');
INSERT INTO exemplaire VALUES (DEFAULT, 1, 'perdu', 'A4');
INSERT INTO exemplaire VALUES (DEFAULT, 2, 'neuf', 'B1');
INSERT INTO exemplaire VALUES (DEFAULT, 3, 'bon', 'B2');
INSERT INTO exemplaire VALUES (DEFAULT, 4, 'abîmé', 'B3');
INSERT INTO exemplaire VALUES (DEFAULT, 5, 'perdu', 'B4');
INSERT INTO exemplaire VALUES (DEFAULT, 6, 'neuf', 'C1');
INSERT INTO exemplaire VALUES (DEFAULT, 7, 'bon', 'C2');

INSERT INTO personne (login, password, email, adresse, nom, prenom, role, dateNaissance) VALUES ('admin', 'admin', 'admin@gmail.com', '1 rue de la paix, 78370 Plaisir', 'admin', 'admin', 'administrateur', '1997-01-01');
INSERT INTO personne VALUES ('adherent1', 'adherent1', 'adherent1@gmail.com', '144 avenue de Versailles 75016 Paris', 'adherent1', 'adherent1', 'adherent', '1997-01-01', '0123456789');
INSERT INTO personne VALUES ('adherent2', 'adherent2', 'adherent2@gmail.com', '144 avenue de Versailles 75016 Paris', 'adherent2', 'adherent2', 'adherent', '1997-08-01', '0123456790');


INSERT INTO emprunt VALUES ('2019-01-01', 1, 'adherent1', '2019-01-08', 22);
INSERT INTO emprunt VALUES ('2019-01-01', 2, 'adherent1', '2019-01-08', 22);
INSERT INTO emprunt VALUES ('2023-12-15', 4, 'adherent1', '2023-12-17', 22);
INSERT INTO emprunt (dateEmprunt, exemplaire, adherent, duree) VALUES ('2023-12-17', 3, 'adherent2', 30);

INSERT INTO suspension VALUES ('2023-12-17', 'adherent1', '2024-01-17', 'dégradation');

INSERT INTO adhesion VALUES ('2019-01-01', 'adherent1', '2020-01-01');
INSERT INTO adhesion (dateDebut, adherent) VALUES ('2021-01-01', 'adherent2');

INSERT INTO edition VALUES (5, 'Gallimard');