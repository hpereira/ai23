# AI23
_Sujet : Biblio_
## Participants
- [Alexandre Bidaux](mailto:alexandre.bidaux@etu.utc.fr)
- [Hans Hookoom](mailto:hans.hookoom@etu.utc.fr)
- [Léon Nguyen](mailto:leon.nguyen@etu.utc.fr)
- [Joyce Tchuendem Docgne](mailto:joyce.tchuendem-docgne@etu.utc.fr)
- [Hugo Pereira](mailto:hugo.pereira@etu.utc.fr)
## Rendu 1
### NDC
La NDC a été réalisée en MarkDown ([Rend.md](Rendu%201/NDC%20V1.md)).
### MCD
Le MCD a été réalisé avec PlantUML ([MCD V4.puml](Rendu%201/MCD%20V4.puml)) et ce dernier a été exporté au format PNG ([MCD V4.png](Rendu%201/MCD%20V4.png)) :

![MCD V4.png](Rendu%201/MCD%20V4.png)

## Rendu 2
### MLD
Le MLD a été réalisé en MarkDown ([MLD.md](Rendu%202/MLD.md))

## Rendu 3
### SQL
-  Création des tables
 - Conception et programmation des contraintes simples et complexes (trigger) 
 - Insertion de données pertinentes

 -> [bd.sql](Rendu%203/bd.sql)

## Rendu 4
### Application Python

#### Bibliothèques utilisées :
- tkinter (pour l'interface graphique)
- psychopg2 (pour se connecter à la base de données PostgreSQL de l'UTC)

#### Lancer l'application :

Sur une machine Linux de l'UTC (les machines sont sous l'OS Debian).

1. Se connecter à son GitLab, dans un terminal : 
```bash
git config --global user.name "votre_user_name" 
git config --global user.email #ici_votre_adresse_mail
```

2. Cloner le répertoire :

_Exécuter la commande suivante dans un terminal :_

```bash
# DISCLAIMER : Le répertoire n'est pas publique. 
# Merci de contacter hpereira@etu.utc.fr

git clone https://gitlab.utc.fr/hpereira/ai23.git
```

3. Installer les dépendances :

_Exécuter la commande suivante dans un terminal (à la racine du projet) :_

```bash
pip3 install -r Rendu\ 4/requirements.txt
```

4. Lancer l'application :

_Exécuter la commande suivante dans un terminal (à la racine du projet) :_

```bash
python3 Rendu\ 4/appli.py
```

Enjoy 😎