import tkinter as tk
from tkinter import ttk, messagebox
from services import DbServices
from models import ressource as r
from functools import partial

class LibraryApp:
    def __init__(self, root):
        self._dbServices = DbServices("dbai23a004", "ai23a004", "l3qP1NMlcjf3")
        self.root = root
        self.root.title("Bibliothèque App")
        self.root.geometry("800x600")

        # Variables pour stocker les informations d'identification
        self.username_var = tk.StringVar()
        self.password_var = tk.StringVar()
        self.state_var = tk.StringVar()

        # Style
        self.style = ttk.Style()
        self.style.configure("TFrame", background="#e1d8b2")
        self.style.configure("TLabel", background="#e1d8b2")
        self.style.configure("TButton", background="#b3b3b3", padding=(10, 5))

        # Menu Bar
        menubar = tk.Menu(self.root)
        self.root.config(menu=menubar)

        file_menu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label="Fichier", menu=file_menu)
        file_menu.add_command(label="Quitter", command=self.root.destroy)

        # Cadre principal pour centrer l'interface
        main_frame = ttk.Frame(self.root)
        main_frame.place(relx=0.5, rely=0.5, anchor="center")

        # Barre de navigation (initialement masquée)
        self.navbar = ttk.Frame(main_frame)

        # Label de bienvenue
        self.welcome_label = ttk.Label(self.navbar, text="", font=("Helvetica", 14))
        self.welcome_label.grid(row=0, column=0, columnspan=3, pady=10)

        ttk.Button(self.navbar, text="Emprunter un document", command=self.borrow_document).grid(row=1, column=0,
                                                                                                 padx=10, pady=10,
                                                                                                 sticky="nsew")
        ttk.Button(self.navbar, text="Rendre un document", command=self.return_document).grid(row=1, column=1, padx=10,
                                                                                              pady=10, sticky="nsew")
        ttk.Button(self.navbar, text="Consulter les documents", command=self.view_document).grid(row=1, column=2,
                                                                                                 padx=10, pady=10,
                                                                                                 sticky="nsew")

        # Ajouter un bouton pour créer un document
        ttk.Button(self.navbar, text="Ajout d'un livre à la bibliothèque", command=self.create_document).grid(row=2, column=1, padx=10, pady=10, sticky="nsew")

        # Ajouter un bouton pour lister tous les utilisateurs
        ttk.Button(self.navbar, text="Lister tous les utilisateurs", command=self.list_all_users).grid(row=3, column=1, padx=10, pady=10, sticky="nsew")

        # Page de connexion (initialement visible)
        self.login_frame = ttk.Frame(main_frame)
        ttk.Label(self.login_frame, text="Nom d'utilisateur:", font=("Helvetica", 12)).grid(row=0, column=0, pady=10,
                                                                                            sticky="e")
        ttk.Entry(self.login_frame, textvariable=self.username_var, font=("Helvetica", 12)).grid(row=0, column=1,
                                                                                                 pady=10, sticky="w")
        ttk.Label(self.login_frame, text="Mot de passe:", font=("Helvetica", 12)).grid(row=1, column=0, pady=10,
                                                                                       sticky="e")
        ttk.Entry(self.login_frame, textvariable=self.password_var, show="*", font=("Helvetica", 12)).grid(row=1,
                                                                                                           column=1,
                                                                                                           pady=10,
                                                                                                           sticky="w")
        ttk.Button(self.login_frame, text="Se connecter", command=self.login, style="TButton").grid(row=2, column=0,
                                                                                                    columnspan=2,
                                                                                                    pady=10)

        # Affiche initialement la page de connexion
        self.show_login()

    def login(self):
        result = self._dbServices.doLogin(self.username_var.get(), self.password_var.get())
        if isinstance(result, str):
            messagebox.showerror("Erreur de connexion", result)
        else:
            self.show_navbar()
            self.welcome_label.config(text="Bonjour {}".format(self.username_var.get()))

    def show_navbar(self):
        self.login_frame.grid_forget()
        self.navbar.grid(row=0, column=0, sticky="nsew")
        self.welcome_label.grid(row=0, column=0, columnspan=3, pady=10)

    def show_login(self):
        self.navbar.grid_forget()
        self.login_frame.grid(row=0, column=0, sticky="nsew")
        self.welcome_label.grid_forget()

    def borrow_document(self):
        documents = self._dbServices.getBorrowableDocuments(self.username_var.get())
        if isinstance(documents, str):
            messagebox.showwarning("Erreur", documents)
        else:
            self.show_documents_window("Documents empruntables", documents, action="emprunter", show_state=True)

    def return_document(self):
        documents = self._dbServices.getReturnableDocuments(self.username_var.get())
        if isinstance(documents, str):
            messagebox.showwarning("Erreur", documents)
        else:
            self.show_documents_window("Documents à retourner", documents, action="rendre", show_state=True)

    def view_document(self):
        documents = self._dbServices.getDocumentsByFilter()
        self.show_documents_window("Tous les documents", documents, enable_filtering=True)

    def show_documents_window(self, title, documents, action=None, show_state=False, enable_filtering=False):
        document_window = tk.Toplevel(self.root)
        document_window.title(title)

        tree = ttk.Treeview(document_window, columns=("Code Unique", "Titre", "Date Apparition", "Genre", "Type", "Langue", "ISBN", "Résumé", "Durée"), show="headings")
        tree.grid(row=0, column=0, padx=10, pady=10, sticky="nsew")

        for col in ("Code Unique", "Titre", "Date Apparition", "Genre", "Type", "Langue", "ISBN", "Résumé", "Durée"):
            tree.heading(col, text=col)
            tree.column(col, width=100)

        for doc in documents:
            tree.insert("", "end", values=(doc.codeunique, doc.titre, doc.dateapparition, doc.genre, doc.type, doc.langue, doc.isbn, doc.resume, doc.duree))

        if show_state and action == "rendre":
            state_label = ttk.Label(document_window, text="État du document:")
            state_label.grid(row=1, column=0, pady=10)
            state_combobox = ttk.Combobox(document_window, textvariable=self.state_var, values=self._dbServices.getEtatsExemplaire())
            state_combobox.grid(row=1, column=1, pady=10)

        if enable_filtering:
            genre_var = tk.StringVar()
            type_var = tk.StringVar()

            def filter_documents():
                selected_genre = genre_var.get() if genre_var.get() != "Tous" else None
                selected_type = type_var.get() if type_var.get() != "Tous" else None

                filtered_documents = self._dbServices.getDocumentsByFilter(genre=selected_genre, type=selected_type)
                self.show_documents_window("Documents filtrés", filtered_documents, enable_filtering=True)

            ttk.Label(document_window, text="Filtrer par genre:").grid(row=2, column=0, pady=10)
            genre_combobox = ttk.Combobox(document_window, textvariable=genre_var, values=["Tous"] + self._dbServices.getGenres())
            genre_combobox.grid(row=2, column=1, pady=10)
            genre_combobox.set("Tous")

            ttk.Label(document_window, text="Filtrer par type:").grid(row=3, column=0, pady=10)
            type_combobox = ttk.Combobox(document_window, textvariable=type_var, values=["Tous"] + self._dbServices.getTypesDocument())
            type_combobox.grid(row=3, column=1, pady=10)
            type_combobox.set("Tous")

            filter_button = ttk.Button(document_window, text="Filtrer", command=filter_documents)
            filter_button.grid(row=4, column=0, columnspan=2, pady=10)

        if action:
            selected_item = tk.StringVar()

            def on_item_selected(event):
                selected_item.set(tree.item(tree.selection())["values"])

            tree.bind("<ButtonRelease-1>", on_item_selected)

            ttk.Button(document_window, text=action.capitalize(), command=partial(self.perform_action, action, selected_item)).grid(row=5, column=0, columnspan=2, pady=10)

        ttk.Button(document_window, text="Fermer", command=document_window.destroy).grid(row=6, column=0, columnspan=2, pady=10)

        for col in ("Titre", "Date Apparition", "Genre", "Type", "Langue", "ISBN", "Résumé", "Durée"):
            tree.column(col, width=ttk.Treeview().column(col, width=None) + 10)

        document_window.geometry("+{}+{}".format(self.root.winfo_x() + 50, self.root.winfo_y() + 50))
        document_window.transient(self.root)
        document_window.grab_set()

    def list_all_users(self):
        # Appelle la fonction de service pour obtenir la liste des utilisateurs
        users = self._dbServices.getAllUsers(self.username_var.get())

        if isinstance(users, str):
            messagebox.showwarning("Erreur", users)
        else:
            # Affiche la liste des utilisateurs dans une nouvelle fenêtre
            self.show_users_window("Liste des Utilisateurs", users)

    def show_users_window(self, title, users):
        users_window = tk.Toplevel(self.root)
        users_window.title(title)

        tree = ttk.Treeview(users_window, columns=("Login", "Role", "Email", "Nom", "Prenom", "Date Naissance", "Telephone"), show="headings")
        tree.grid(row=0, column=0, padx=10, pady=10, sticky="nsew")

        for col in ("Login", "Role", "Email", "Nom", "Prenom", "Date Naissance", "Telephone"):
            tree.heading(col, text=col)
            tree.column(col, width=100)

        for user in users:
            tree.insert("", "end", values=(user.login, user.role, user.email, user.nom, user.prenom, user.date_naissance, user.telephone))

        ttk.Button(users_window, text="Fermer", command=users_window.destroy).grid(row=1, column=0, pady=10)

    def create_document(self):
        # Vérifiez si l'utilisateur actuel est un administrateur
        if not self._dbServices.userIsAdmin(self.username_var.get()):
            messagebox.showwarning("Permission refusée", "Vous n'avez pas les droits pour créer un document.")
            return

        # Affiche une fenêtre pour saisir les informations du nouveau document
        create_document_window = tk.Toplevel(self.root)
        create_document_window.title("Créer un document")

        # Ajoutez des Entry pour les détails du document (titre, genre, ISBN, etc.)
        ttk.Label(create_document_window, text="Titre:").grid(row=0, column=0, pady=5)
        title_entry = ttk.Entry(create_document_window)
        title_entry.grid(row=0, column=1, pady=5)

        ttk.Label(create_document_window, text="Genre:").grid(row=1, column=0, pady=5)
        genre_entry = ttk.Entry(create_document_window)
        genre_entry.grid(row=1, column=1, pady=5)

        ttk.Label(create_document_window, text="ISBN:").grid(row=2, column=0, pady=5)
        isbn_entry = ttk.Entry(create_document_window)
        isbn_entry.grid(row=2, column=1, pady=5)

        ttk.Label(create_document_window, text="Date Apparition:").grid(row=3, column=0, pady=5)
        dateApparition_entry = ttk.Entry(create_document_window)
        dateApparition_entry.grid(row=3, column=1, pady=5)

        ttk.Label(create_document_window, text="Langue:").grid(row=4, column=0, pady=5)
        langue_entry = ttk.Entry(create_document_window)
        langue_entry.grid(row=4, column=1, pady=5)

        ttk.Label(create_document_window, text="Résumé:").grid(row=5, column=0, pady=5)
        resume_entry = ttk.Entry(create_document_window)
        resume_entry.grid(row=5, column=1, pady=5)

        # Ajoutez un bouton pour effectuer la création du document
        ttk.Button(create_document_window, text="Créer", command=lambda: self.perform_create_document(title_entry.get(), genre_entry.get(), isbn_entry.get(), dateApparition_entry.get(), langue_entry.get(), resume_entry.get())).grid(row=6, column=0, columnspan=2, pady=10)

    def perform_create_document(self, title, genre, isbn, dateApparition, langue, resume):
        # Créez un objet Ressource avec les détails saisis
        new_document = r.Ressource(codeunique=0, titre=title, genre=genre, isbn=isbn, dateapparition=dateApparition, duree=0, langue=langue, resume=resume, type="Livre")

        # Appelez la fonction correspondante dans le service de base de données
        result = self._dbServices.createDocument(self.username_var.get(), new_document)

        # Affichez le résultat de la création
        messagebox.showinfo("Création de document", result)

        # Fermez la fenêtre de création du document
        self.root.update()


    def perform_action(self, action, selected_item):
        if action == "emprunter":
            result = self._dbServices.borrowDocument(self.username_var.get(), selected_item.get().split('(')[1].split(',')[0])
            if isinstance(result, str):
                messagebox.showwarning("Erreur", result)
            else:
                messagebox.showinfo("Succès", "{} effectué avec succès".format(action.capitalize()))
                self.borrow_document()  # Rafraîchir la liste après emprunt
        elif action == "rendre":
            result = self._dbServices.returnDocument(self.username_var.get(), selected_item.get().split('(')[1].split(',')[0], self.state_var.get())
            if isinstance(result, str):
                messagebox.showwarning("Erreur", result)
            else:
                messagebox.showinfo("Succès", "{} effectué avec succès".format(action.capitalize()))
                self.return_document()  # Rafraîchir la liste après retour
        selected_item.set("")
        self.root.update()

if __name__ == "__main__":
    root = tk.Tk()
    app = LibraryApp(root)
    root.mainloop()

