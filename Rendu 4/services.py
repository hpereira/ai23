import psycopg2
import datetime as date
from models import personne as p
from models import ressource as r
from models import contributeur as c
from models import exemplaire as e


class DbServices:
    _host = "tuxa.sme.utc"

    def __init__(self, db, user, pwd):
        self.connection = psycopg2.connect(
            "dbname='%s' user='%s' host='%s' password='%s'" % (db, user, DbServices._host, pwd)
        )

    def doLogin(self, login, pwd):
        cur = self.connection.cursor()

        # We check if the user exists
        sql = "SELECT password, role, login, email, adresse, nom, prenom, datenaissance, telephone FROM personne WHERE login='%s'" % login
        cur.execute(sql)
        raw = cur.fetchone()
        if not raw:
            return "Compte inconnu"
        if pwd != raw[0]:
            return "Mauvais mot de passe"
        else:
            adhesion = self.getUserLastAdhesionEndDate(login)
            if (adhesion and not adhesion[0]) or self.userIsAdmin(login) : #adhesion pas finie
                return p.Personne(raw[2], raw[1], raw[3], raw[4], raw[5], raw[6], raw[7], adhesion[0] if adhesion else adhesion)
            if not adhesion:
                return f"Veuillez adhérer au service pour y avoir accès"
            else:
                return f"Vous n'êtes plus adherent depuis le {adhesion[0]}"

    def getBorrowableDocuments(self, login, type=None):
        cur = self.connection.cursor()

        # We start by checking if the user isn't suspended
        raw = self.getUserSuspension(login)
        if raw:
            return "Impossible d'emprunter, vous êtes suspendu %s (%s)" % (
                "de manière indéterminée" if raw[0] is None else "jusqu'au %s" % raw[0], raw[1])

        # User is OK to borrow a book, get the correct type of ressources
        sql = ("SELECT codeunique, titre, dateapparition, genre, type, langue, isbn, resume, duree "
               "FROM %s "
               "WHERE codeunique IN (SELECT DISTINCT exemplaire.ressource "
               "    FROM exemplaire "
               "    WHERE exemplaire.ressource=%s.codeunique AND (etat='bon' OR etat='neuf'))"
               "    AND codeunique NOT IN (SELECT DISTINCT codeunique FROM exemplaires_empruntes)"
               f"    AND codeunique NOT IN (SELECT exemplaire FROM emprunt WHERE dateemprunt=now()::date AND adherent='{login}')")
        if type is None:
            sql = (sql % ("ressource", "ressource")) + " ORDER BY type"
        elif type == "Film" or type == "Livre" or type == "EnregistrementAudio":
            sql = sql % (type.lower(), type.lower())
        else:
            return "%s is an unknown type" % type

        cur.execute(sql)
        res = cur.fetchall()
        if res:
            listOfDocs = []
            for raw in res:
                listOfDocs.append(r.Ressource(raw[0], raw[1], raw[2], raw[3], raw[4], raw[5], raw[6], raw[7], raw[8]))
            return listOfDocs
        else:
            return "Aucun document disponible a l'emprunt"

    def getUserSuspension(self, login):
        cur = self.connection.cursor()
        sql = "SELECT datefin, motif FROM suspension WHERE adherent='%s' AND (datefin IS NULL OR datefin > now())" % login
        cur.execute(sql)
        raw = cur.fetchone()
        return raw

    def getUserLastAdhesionEndDate(self, login):
        cur = self.connection.cursor()
        sql = "SELECT datefin FROM adhesion WHERE adherent='%s' ORDER BY datedebut DESC " % login
        cur.execute(sql)
        raw = cur.fetchone()
        return raw

    def getReturnableDocuments(self, login):
        cur = self.connection.cursor()
        sql = ("SELECT r.codeunique, r.titre, r.dateapparition, r.genre, r.type, r.langue, r.isbn, r.resume, r.duree "
               "FROM (exemplaire ex INNER JOIN emprunt em ON ex.codeunique=em.exemplaire) "
               "    INNER JOIN ressource r ON r.codeunique=ex.ressource "
               "WHERE em.adherent='%s' AND (em.daterendu IS NULL)") % login
        cur.execute(sql)
        res = cur.fetchall()
        if not res:
            return "Vous n'avez aucun document à rendre"
        listOfDocs = []
        for raw in res:
            listOfDocs.append(r.Ressource(raw[0], raw[1], raw[2], raw[3], raw[4], raw[5], raw[6], raw[7], raw[8]))
        return listOfDocs

    def getDocumentsByFilter(self, genre=None, type=None):
        cur = self.connection.cursor()
        sql = ("SELECT codeunique, titre, dateapparition, genre, type, langue, isbn, resume, duree "
               "FROM ressource ")
        if genre is not None or type is not None:
            sql += "WHERE "
        if genre is not None:
            sql += "genre = '%s' " % type
        if type is not None:
            sql += "type = '%s' " % type
        sql += "ORDER BY titre ASC"

        cur.execute(sql)
        res = cur.fetchall()
        if not res:
            return "Aucun document trouvé avec ces critères"
        listOfDocs = []
        for raw in res:
            listOfDocs.append(r.Ressource(raw[0], raw[1], raw[2], raw[3], raw[4], raw[5], raw[6], raw[7], raw[8]))
        return listOfDocs

    def getGenres(self):
        cur = self.connection.cursor()
        sql = "SELECT nom FROM genre ORDER BY nom ASC"

        cur.execute(sql)
        res = cur.fetchall()
        if not res:
            return "Erreur dans la connexion a la BDD, aucun genre trouvé"
        listOfGenres = []
        for raw in res:
            listOfGenres.append(raw[0])
        return listOfGenres

    def getTypesDocument(self):
        cur = self.connection.cursor()
        sql = "SELECT DISTINCT type FROM ressource ORDER BY type ASC"

        cur.execute(sql)
        res = cur.fetchall()
        if not res:
            return "Erreur dans la connexion a la BDD, aucun type trouvé"
        listOfTypes = []
        for raw in res:
            listOfTypes.append(raw[0])
        return listOfTypes

    def getEtatsExemplaire(self):
        cur = self.connection.cursor()
        sql = "SELECT DISTINCT etat FROM exemplaire ORDER BY etat ASC"

        cur.execute(sql)
        res = cur.fetchall()
        if not res:
            return "Erreur dans la connexion a la BDD, aucun etat trouvé"
        listOfEtats = []
        for raw in res:
            listOfEtats.append(raw[0])
        return listOfEtats

    def borrowDocument(self, login, codeunique):
        cur = self.connection.cursor()

        # On verifie si l'utilisateur est susependu
        raw = self.getUserSuspension(login)
        if raw:
            return "Impossible d'emprunter, vous êtes suspendu %s (%s)" % (
                "de manière indéterminée" if raw[0] is None else "jusqu'au %s" % raw[0], raw[1])

        # On recupere un exemplaire de la ressource si possible
        sql = ("SELECT codeunique "
               "FROM exemplaire "
               "WHERE ressource=%s AND (codeunique NOT IN (select codeunique FROM exemplaires_empruntes)) AND (etat = 'neuf' OR etat = 'bon')" % codeunique)

        cur.execute(sql)
        raw = cur.fetchone()
        if not raw:
            return "Aucun exemplaire de cettre ressource est disponible"
        exemplaire = raw[0]

        # On vérifie le nombre d'emprunts de documents
        sql = ("SELECT count(adherent) "
               "FROM emprunt "
               "WHERE adherent='%s' AND daterendu IS NULL " % login)

        cur.execute(sql)
        raw = cur.fetchone()
        print(raw[0])
        if raw and raw[0] >= 5:
            return "Vous avez déjà atteint le maximum de 5 emprunts"

        sql = ("INSERT INTO emprunt(exemplaire, adherent, dateemprunt, duree) VALUES(%s , '%s', now(), 7)" % (
            codeunique, login))
        cur.execute(sql)
        self.connection.commit()

    def returnDocument(self, login, codeunique, etat=None):
        cur = self.connection.cursor()

        sql = ("SELECT exemplaire "
               "FROM emprunt "
               "WHERE adherent='%s' AND daterendu IS NULL "
               "AND exemplaire IN (SELECT codeunique FROM exemplaire WHERE ressource=%s)") % (login, codeunique)

        cur.execute(sql)
        raw = cur.fetchone()
        if not raw:
            return "Vous n'avez pas emprunté ce document, vous ne pouvez donc pas le rendre"

        exemplaire = raw[0]
        sql = "UPDATE emprunt SET daterendu=now() WHERE adherent='%s' AND exemplaire=%d" % (login, exemplaire)
        cur.execute(sql)
        print(sql)
        # On analyse la date de rendu et le retard potentiel
        sql = "SELECT dateemprunt, duree FROM emprunt WHERE  adherent='%s' AND exemplaire=%s ORDER BY dateemprunt" % (login, exemplaire)
        cur.execute(sql)
        row = cur.fetchone()
        duree_initiale_emprunt = int(row[1])
        date_emprunt = row[0]
        date_prevue_rendu = date_emprunt + date.timedelta(days=duree_initiale_emprunt)
        date_reelle_rendu = date.datetime.today().date()
        duree_reelle_emprunt = date_reelle_rendu - date_prevue_rendu

        if duree_reelle_emprunt.days > duree_initiale_emprunt:
            delta_date = (date.datetime.today() + date.timedelta(days=(duree_reelle_emprunt.days-duree_initiale_emprunt))).date()
            sql = ("INSERT INTO suspension(adherent, datedebut, motif, datefin) VALUES('%s', now(), '%s', '%s')"
                   % (login, "retard", delta_date))
            cur.execute(sql)

        if etat is not None and etat != '':
            sql = ("UPDATE exemplaire SET etat='%s' WHERE codeunique=%s" % (etat, exemplaire))
            cur.execute(sql)

            if etat == "perdu" or etat == "abîmé":
                sql = ("INSERT INTO suspension(adherent, datedebut, motif) VALUES('%s', now(), '%s')" % (login, "perte" if etat == "perdu" else "dégradation"))
                cur.execute(sql)
        self.connection.commit()

    def updateDocument(self, login, document: r.Ressource):
        cur = self.connection.cursor()

        sql = "SELECT role FROM personne WHERE login=%s" % login

        cur.execute(sql)
        raw = cur.fetchone()
        if raw[0] != "admin":
            return "Vous n'avez pas le droit de faire cette modification"

        sql = ("UPDATE ressource "
               "SET titre=%s, genre=%s, isbn=%s, dateapparition=%s, duree=%d, langue=%s, resume=%s"
               "WHERE codeunique=%d" % (
                   document.titre, document.genre, document.isbn, document.dateapparition, document.duree, document.langue,
                   document.resume, document.codeunique))
        cur.execute(sql)
        self.connection.commit()

    def createDocument(self, login, document: r.Ressource):
        if not self.userIsAdmin(login):
            return "Vous n'avez pas le droit de faire cette creation"

        cur = self.connection.cursor()
        sql = (("INSERT INTO ressource(titre, genre, isbn, dateapparition, duree, langue, resume, type) VALUES ('%s', '%s', '%s', '%s', %d, '%s', '%s', '%s')") %
               (document.titre, document.genre, document.isbn, document.dateapparition, document.duree, document.langue,document.resume, "Livre"))
        cur.execute(sql)
        self.connection.commit()

    def getAllUsers(self, login):
        if not self.userIsAdmin(login):
            return "Vous n'avez pas le droit de faire cette requête"

        cur = self.connection.cursor()
        sql = "SELECT login, role, email, adresse, nom, prenom, datenaissance, telephone FROM personne"
        cur.execute(sql)
        res = cur.fetchall()
        list = []
        for row in res:
            list.append(p.Personne(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7]))
        return list

    def userIsAdmin(self, login):
        cur = self.connection.cursor()

        sql = "SELECT role FROM personne WHERE login='%s'" % login

        cur.execute(sql)
        raw = cur.fetchone()
        return raw and (raw[0] == "administrateur")

    def getContributeursOnRessource(self, codeunique):
        cur = self.connection.cursor()

        sql = ("SELECT contributeur.id, contributeur.nom, contributeur.prenom, contributeur.datenaissance, contributeur.pays, contributeur.type "
               "FROM contributeur INNER JOIN contribution ON contributeur.id = contribution.contributeur "
               "WHERE contribution.ressource='%s'") % codeunique

        cur.execute(sql)
        res = cur.fetchall()
        list = []
        for row in res:
            list.append(c.Contributeur(row[0], row[1], row[2], row[3], row[4], row[5]))
        return list

    def getExemplairesOfRessource(self, codeunique):
        cur = self.connection.cursor()

        sql = ("SELECT codeunique, etat, codeposition, ressource "
               "FROM exemplaire "
               "WHERE ressource='%s'") % codeunique

        cur.execute(sql)
        res = cur.fetchall()
        list = []
        for row in res:
            list.append(e.Exemplaire(row[0], row[1], row[2], row[4]))
        return list

    def __del__(self):
        self.connection.close()

