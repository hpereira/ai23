class Ressource:
    def __init__(self, codeunique, titre, dateapparition, genre, type, langue, isbn, resume, duree):
        self.codeunique = codeunique
        self.titre = titre
        self.dateapparition = dateapparition
        self.genre = genre
        self.type = type
        self.langue = langue
        self.isbn = isbn
        self.resume = resume
        self.duree = duree
