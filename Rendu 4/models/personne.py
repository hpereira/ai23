class Personne:
    def __init__(self, login = None, role = None, email = None, adresse = None, nom = None, prenom = None, date_naissance = None, telephone = None, dateFinAdhesion = None):
        self.login = login
        self.role = role
        self.email = email
        self.adresse = adresse
        self.nom = nom
        self.prenom = prenom
        self.date_naissance = date_naissance
        self.telephone = telephone
        self.dateFinAdhesion = dateFinAdhesion