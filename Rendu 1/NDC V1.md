# Projet Biblio - NDC V1

## Légende
* **attribut**
* ***clef***
* [lien vers une entité](#legende)
* <u>[INTERPRETATION]</u>

## NDC

### Pays
* Est identifié par son ***nom***
* Est lié à 0 ou plusieurs [Contributeurs](#contributeur)

### Contributeur
* Est identifié par son ***nom***, ***prénom*** et ***date de naissance***
* A une seule nationalité [Pays](#pays) 
* A crée 0 ou plusieurs [Ressources](#ressource)
* Joue dans 0 ou plusieurs [Films](#film) 
* Est interprète dans 0 ou plusieurs [Enregistrements Audios](#enregistrement-audio)

### Editeur
* Est identifié par son ***nom***
* A édité 0 ou plusieurs [Ressources](#ressource)

### Genre
* Est identifié par son ***nom***
* Qualifie 0 ou plusieurs [Ressources](#ressource)

### Ressource
* Est identifiée par son ***code unique***
* A un **titre** et une **date d'apparition**
* A été crée pas au moins un [Contributeur](#contributeur)
* Est édité par au moins un [Editeur](#editeur)
* Appartient à un seul [Genre](#genre)
* Est matérialisé sous la forme de 0 ou plusieurs [Exemplaires](#exemplaire)
* Doit se décliner sous la forme d'un[Livre](#livre), [Film](#film) ou [Enregistrement Audio](#enregistrement-audio) (Héritage total et exclusif)

### Exemplaire
* Est identifié par son ***code unique*** de [Ressource](#ressource) et par son ***code de position*** (clef locale)
* A un etat pouvant être soit : neuf, bon, abîmé, perdu
* Est composé par une [Ressource](#ressource)
* Peut être emprunté par un [Adhérent](#adhérent) à la fois
* <u>[INTERPRETATION]</u> Le ***code de position*** représente la position courante de l'exemplaire dans la bibliothèque. Ainsi, chaque exemplaire a une position différente lui permettant d'être identifié par rapport aux autres

### Livre
* Est une [Ressource](#ressource) (héritage, sous-type)
* Est identifié par son ***code unique*** de [Ressource](#ressource) et par son ***ISBN*** 
* A un **résumé**
* Est écrit en une seule [Langue](#langue)

### Film
* Est une [Ressource](#ressource) (héritage, sous-type)
* Est identifié par son ***code unique*** de [Ressource](#ressource)
* A une **durée** et un **synopsis**
* Est joué par un ou plusieurs [Acteurs](#contributeur) (Contributeurs)
* <u>[INTERPRETATION]</u> Est sous-titré en une seule [Langue](#langue) (la description du client ne précise pas si les films ont plusieurs langues, on part du principe que non)


### Enregistrement Audio
* Est une [Ressource](#ressource) (héritage, sous-type)
* Est identifié par son ***code unique*** de [Ressource](#ressource)
* A une **durée**
* Est interprété par un ou plusieurs [Interprètes](#contributeur) (Contributeurs)

### Langue
* Est identifiée par son ***nom***
* Est utilisée dans 0 ou plusieurs [Livres](#livre)  
* Est utilisée dans 0 ou plusieurs [Films](#livre)  

### Personne
* Est identifiée par un ***login***, un ***email*** ou par son **nom**/ **prénom**/**adresse**
* A un **mot de passe** pour se connecter
* Une personne doit être soit un [Administrateur](#administrateur), soit un [Adhérent](#adhérent) soit les deux
* Deux [Personnes](#personne) ne peuvent pas avoir le même triplet **nom**/ **prénom**/**adresse** (unique à chacun)
* <u>[INTERPRETATION]</u> Il y a trois manières d'identifier une personne : son ***login***, son ***email*** et son triplet ***nom***/***prenom***/***adresse***. Nous pensons que le ***login*** est la meilleure manière d'identifier la [Personne](#personne), faisant de cet attribut la clef primaire 

### Administrateur
* Est une [Personne](#personne) (héritage, sous-type)
* Est identifié par son ***login*** de [Personne](#personne)

### Adhérent
* Est une [Personne](#personne) (héritage, sous-type)
* Est identifié par son ***login*** de [Personne](#personne) et son ***numéro de téléphone*** 
* A une **date de naissance**
* Doit avoir au moins une [Adhésion](#adhésion) 
* Peut avoir 0 ou plusieurs [Suspensions](#suspension) 
* Peut emprunter entre 0 et un nombre limité d'[Exemplaires](#exemplaire)

### Adhésion
* Est identifié par le **login** d'un [Adhérent](#adhérent) et par sa **date de début**
* Concèrne obligatoirement un seul [Adhérent](#adhérent)
* A une **date de fin** pouvant être nulle ([Adhésion](#adhésion) non clôturée)
* Si une **date de fin** existe, alors elle ne peut pas précéder la **date de début** de l'[Adhésion](#adhésion) 
* <u>[INTERPRETATION]</u> Tant que l'[Adhérent](#adhérent) ne clôt pas son [Adhésion](#adhésion), alors il n'existe pas de **date de fin** (on pourrait également borner à un an la fin d'une [Adhésion](#adhésion), forçant la **date de fin** de l'[Adhésion](#adhésion) à être définie)
* <u>[INTERPRETATION]</u> Comme nous voulons concerver toutes les traces d'[Adhésions](#adhésion)  passées et présentes, nous n'avons pas fait de relation de composition entre l'[Adhérent](#adhérent) et l'[Adhésion](#adhésion) car cela impliquerait qu'une suppression de l'[Adhérent](#adhérent) supprimerait les [Adhésions](#adhésion) correspondantes

### Suspension
* Est identifié par le **login** d'un [Adhérent](#adhérent) et par sa **date de début**
* Concèrne obligatoirement un seul [Adhérent](#adhérent)
* A une **date de fin** pouvant être nulle ([Adhérent](#adhérent) n'ayant pas remboursé un [Exemplaire](#exemplaire) abimé/perdu)
* Si une **date de fin** existe, alors elle ne peut pas précéder la **date de début** de la [Suspension](#suspension)
* Si un [Adhérent](#adhérent) a trop de [Suspensions](#suspension), alors il est blacklisté (ne peut plus réaliser d'[Emprunts](#emprunt)) 

### Emprunt
* Concèrne un [Adhérent](#adhérent) et un [Exemplaire](#exemplaire)
* Est identifié par le **login** d'un [Adhérent](#adhérent), le **codeUnique** et le **codePosition** d'un [Exemplaire](#exemplaire) et par sa **date de début**
* Il est impossible d'avoir deux [Emprunts](#emprunt) avec le même triplet **codeUnique**/**codePosition**/**date de début** (pour éviter l'[Emprunt](#emprunt) simultané du même [Exemplaire](#exemplaire))
* L'[Emprunt](#emprunt) ne peut concerner que des [Exemplaires](#exemplaire) neufs ou bons
* L'[Emprunt](#emprunt) ne peut concerner que des [Adhérents](#adhérent) non-suspendus et non-blacklistés 
* <u>[INTERPRETATION]</u> Un [Adhérent](#adhérent) devrait pouvoir réaliser plusieurs [Emprunts](#emprunt) du même [Exemplaire](#exemplaire) à des dates différentes. C'est pour cette raison qu'on identifie l'[Emprunt](#emprunt) par le **login** d'un [Adhérent](#adhérent), le **codeUnique** et le **codePosition** d'un [Exemplaire](#exemplaire) et par une **date de début**
* <u>[INTERPRETATION]</u> Maximum 5 [Emprunts](#emprunt) simultanés pour un [Adhérent](#adhérent)
* <u>[INTERPRETATION]</u> Un [Emprunt](#emprunt) dure 7 jours 