# Comment ouvrir le _plantUML_
Pour modifier le MCD : 
- Ouvrir le projet sur _Intellij_ (File --> Open)
- Installer _plantUML_ sur _Intellij_ (Settings --> Plugins)
- Ouvrir le fichier [puml](MCD%20V4.puml)