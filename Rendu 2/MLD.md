# Projet Biblio - MLD V1

LES CONTRAINTES SONT À AJOUTER !!!
## Légende
* **Table**
* _unique_ (voir si nécessaire)

## MLD

**Pays**(#nom:string)

___

**Genre**(#nom:string)

___

**Ressource**(#codeUnique:int, titre:string, dateApparition:Date, genre=>Genre, type:{EnregistrementAudio, Film, Livre}, langue=>Langue, isbn:string, resume:string, duree:int) 

Avec :
* genre NOT NULL
* type NOT NULL
* titre NOT NULL
* dateApparition NOT NULL 
* IF type = Film THEN resume NOT NULL AND duree NOT NULL AND langue NOT NULL
* IF type = Livre THEN resume NOT NULL AND isbn NOT NULL AND langue NOT NULL
* IF type = EnregistrementAudio THEN duree NOT NULL

___

**Contributeur**(#id:int, nom:string, prenom:string, type: {Acteur Auteur, Realisateur, Interprete, Compositeur} dateNaissance:Date, pays=>Pays) 

Avec :
* pays NOT NULL
* (nom, prenom, dateNaissance, type) KEY (NOT NULL & UNIQUE) (permet d'avoir la même personne comme étant plusieurs type de contributeur en même temps)
___

<!-- Ici les personnes qui vont créer la ressource 
- une seule personne ne peut créer une ressource particuliere qu'une seule fois -->
**Contribution**(date:Date, #contributeur=>Contributeur(id), #ressource=>Ressource(codeUnique))

Contraintes complexes :
* Une ressource à forcément un contributeur
  * Projection(Ressource, codeUnique) = Projection(Contribution, ressource)

* Les contributeurs qui contribuent à un livre sont des auteurs
  * CONTRIBUTEURS_AUTEURS = Projection(Restriction(Contributeur, type='Auteur'), id) 
  * RESSOURCES_LIVRES = Projection(Restriction(Ressource, type='Livre'), codeUnique)
  * Restriction(Contribution, ressource IN RESSOURCES_LIVRES AND contributeur NOT IN CONTRIBUTEURS_AUTEUR) = {}

* Les contributeurs qui contribuent à un film sont des réalisateurs ou des acteurs 
  * CONTRIBUTEURS_REAL_ACT = Projection(Restriction(Contributeur, type='Realisateur' OR type='Acteur'), id) 
  * RESSOURCES_FILMS = Projection(Restriction(Ressource, type='Film'), codeUnique)
  * Restriction(Contribution, ressource IN RESSOURCES_FILMS AND contributeur NOT IN CONTRIBUTEURS_REAL_ACT) = {}

* Les contributeurs qui contribuent à un enregistrement audio sont des compositeurs ou des interprètes 
  * CONTRIBUTEURS_CONTRIB_INTER = Projection(Restriction(Contributeur, type='Compositeur' OR type='Interprete'), id) 
  * RESSOURCES_ENREGISTREMENT = Projection(Restriction(Ressource, type='EnregistrementAudio'), codeUnique)
  * Restriction(Contribution, ressource IN RESSOURCES_ENREGISTREMENT AND contributeur NOT IN CONTRIBUTEURS_CONTRIB_INTER) = {}
___

**Editeur**(#nom:string)
___

**Edition**(#ressource=>Ressource(codeUnique),#editeur=>Editeur(nom))

Contrainte complexe :
* Une ressource a forcément une édition
  *  Projection(Ressource, codeUnique) = Projection(Edition, ressource)

___

**Exemplaire**(#id:int, ressource=>Ressource(codeUnique), etat:{neuf, bon, abîmé, perdu}, codePosition:string)

Avec :
* etat NOT NULL
* codePosition KEY (UNIQUE & NOT NULL)
___

**Personne**(#login:string, password:string, _email:string_, _adresse:string, nom:string, prenom:string_, role:{adherent, administrateur}, dateNaissance:Date, _telephone:CHAR(10)_)

Avec :
* password NOT NULL
* (nom, prenom, adresse) KEY (UNIQUE & NOT NULL)
* email KEY (UNIQUE & NOT NULL)
* tel UNIQUE
* role NOT NULL
* IF role = adherent THEN telephone NOT NULL AND dateNaissance NOT NULL
___

**Emprunt**(#dateEmprunt:Date, #exemplaire=>Exemplaire(id),
#adherent=>Adherent(login), dateRendu:Date, duree:int)

Avec :
* duree NOT NULL
* dateRendu IS NULL OR dateRendu >= dateEmprunt
* exemplaire.etat <> abîmé AND exemplaire.etat <> perdu (a voir car peut poser probleme sur les historiques en fonction du SGBD)
* Un adherent ne peut avoir que 5 emprunts simultanés (emprunt en cours : dateRendu IS NULL)

___

**Suspension**(#dateDebut:Date, #adherent=>Adherent(login), dateFin:Date, motif: {dégradation, perte, retard, blacklist})

Avec : 
* dateFin IS NULL OR dateFin >= dateDebut
* motif NOT NULL

___

**Adhesion**(#dateDebut:Date, #adherent=>Adherent(login), dateFin:Date)

Avec :
* dateFin IS NULL OR dateFin >= dateDebut